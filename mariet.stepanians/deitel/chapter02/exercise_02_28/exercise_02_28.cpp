#include <iostream>

int
main()
{
    int fiveDigitNumber;
    int digit1;
    int digit2;
    int digit3;
    int digit4;
    int digit5;
    
    std::cout << "Enter a five-digit number: ";
    std::cin  >> fiveDigitNumber;
    
    if (fiveDigitNumber > 99999) {
    	std::cout << "Not a five digit number!" << std::endl;
    	return 1;
    }
    
    if (fiveDigitNumber < 10000) {
	std::cout << "Not a five digit number!" << std::endl;
	return 1; ///return error
    }
    
    digit1 = (fiveDigitNumber / 10000);
    digit2 = (fiveDigitNumber / 1000) % 10;
    digit3 = (fiveDigitNumber / 100) % 10;
    digit4 = (fiveDigitNumber / 10) % 10;
    digit5 = fiveDigitNumber % 10;
    
    std::cout << " " << digit1;
    std::cout << " " << digit2;
    std::cout << " " << digit3;
    std::cout << " " << digit4;
    std::cout << " " << digit5 << std::endl;
    
    
    return 0;
}
