#include <iostream>
#include <limits.h>

int
main()
{
    int numberCount;
    std::cout << "Enter number: ";
    std::cin  >> numberCount;

    if (numberCount < 1) {
        std::cerr << "Error 1: Number can not be negative or equal in 0." << std::endl;
        return 1;
    }

    int min = INT_MAX;
    for (int i = numberCount; i > 0; --i) {
        int number;
        std::cout << "Enter number: ";
        std::cin  >> number;
        
        if (min > number) {
            min = number;
        }
    }

    std::cout << "Minimum is " << min << std::endl;
    return 0;
}

