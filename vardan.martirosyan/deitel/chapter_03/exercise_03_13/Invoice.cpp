#include "Invoice.hpp"
#include <iostream>
#include <string>

Invoice::Invoice(std::string number, std::string description, int quantity, int price)
{
    setNumber(number);
    setDescription(description);
    setQuantity(quantity);
    setPrice(price);
}

void
Invoice::setNumber(std::string number)
{
    number_ = number;
}

std::string Invoice::getNumber()
{
    return number_;
}

void
Invoice::setDescription(std::string description)
{
    description_ = description;
}

std::string Invoice::getDescription()
{
    return description_;
}

void 
Invoice::setQuantity(int quantity)
{
    if (quantity < 0) {
        quantity_ = 0;
        std::cout << "Info 1: The quantity is not positive, it is setted to 0." << std::endl;
        return;
    }
    quantity_ = quantity;
}

int 
Invoice::getQuantity()
{
    return quantity_;
}

void
Invoice::setPrice(int price)
{
    if (price < 0) {
        price_ = 0;
        std::cout << "Info 2: The price is not positive, it is setted to 0." << std::endl;
        return;
    }
    price_ = price;
}

int
Invoice::getPrice()
{
    return price_;
}

int
Invoice::getInvoiceAmount()
{
    return getQuantity() * getPrice();
}

