#include<iostream>

int
main()
{
    int number1;
    int number2;
    int number3;
    
    std::cout << "Input three different integers: ";
    std::cin >> number1 >> number2 >> number3;

    std::cout << "Sum is: " << number1 + number2 + number3 << "\n"
              << "Average is: " << (number1 + number2 + number3)/3 << "\n"
              << "Product is: " << number1 * number2 * number3 << "\n"; 

    int min = number1;
    int max = number1;

    if (min > number2) {
        min = number2;
    }
    if (min > number3) {
        min = number3;
    }

    std::cout << "Smallest is: " << min << "\n";

    if (max < number2) {
        max = number2;
    }
    if (max < number3) {
        max = number3;
    }

    std::cout << "Largest is: " << max << std::endl;
    
    return 0;
}
