#include <iostream>

int
main()
{
    double priceOfAllSoldForWeek = 0;

    for (int day = 1; day <= 7; ++day) {
        int productNumber;
        std::cin >> productNumber;
        if (productNumber < 1) {
            std::cout << "Error 1: wrong product number." << std::endl;
            return 1;
        }
        if (productNumber > 5) {
            std::cout << "Error 1: wrong product number." << std::endl;
            return 1;
        }

        int quantitySoldPerDay;
        std::cin >> quantitySoldPerDay;
        if (quantitySoldPerDay < 0) {
            std::cout << "Error 2: wrong quantity." << std::endl;
            return 2;
        }

        switch (productNumber) {
        case 1: priceOfAllSoldForWeek += quantitySoldPerDay * 2.98; break;
        case 2: priceOfAllSoldForWeek += quantitySoldPerDay * 4.50; break;
        case 3: priceOfAllSoldForWeek += quantitySoldPerDay * 9.98; break;
        case 4: priceOfAllSoldForWeek += quantitySoldPerDay * 4.49; break;
        case 5: priceOfAllSoldForWeek += quantitySoldPerDay * 6.87; break;
        }
    }
    std::cout << priceOfAllSoldForWeek << "$" << std::endl;

    return 0;
}

