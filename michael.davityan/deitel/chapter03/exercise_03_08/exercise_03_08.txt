A)Header file is the file, that contains function prototypes or class definitions(with its own function prototypes and member-variables).

B)We can see two type of source-code file.

First: it can be the source file that contains functions or classes descriptions, that we define in the header file.
Second: Or it can be the source-code file where we do our job in main function.

We include the header files of classes and functions in our source file that contain main function, and can use all 
the classes and functions defined in header file.

Its much more effective to use header files that contains functions or classes in our main source file, than if we 
define and wrote all the description of functions and classes in our main source code, because in that way we can 
build a huge library of different header files that contains different functions and classes, and use it to build the 
objects that we want and not overwrite all again.
