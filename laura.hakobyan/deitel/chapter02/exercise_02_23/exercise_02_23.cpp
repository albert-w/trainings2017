/// print smallest and largest of 5 numbers
#include <iostream>

int 
main() 
{ 
    int a, b, c, d, e;
    std::cout << "Type 5 numbers: " << "\n";
    std::cin >> a >> b >> c >> d >> e;

    ///Smallest number
    int small = a;
    if (b < small) {
        small = b;
    }
    if (c < small) {
        small = c;
    }
    if (d < small) {
        small = d;
    }
    if (e < small) {
        small = e;
    }
    std::cout << "Smallest number: " << small << std::endl;
    
    /// Largest number
    int large = a;
    if (b > large) {
        large = b;
    }
    if (c > large) {
        large = c;
    }
    if (d > large) {
        large = d;
    }
    if (e > large) {
        large = e;
    }
    std::cout << "Largest number: " << large << std::endl;

    return 0;
}

