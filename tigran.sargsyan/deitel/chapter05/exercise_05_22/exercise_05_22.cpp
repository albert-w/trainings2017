#include <iostream>

int
main()
{
    {
        int x, y;
        std::cout << "Enter x: ";
        std::cin >> x;
        std::cout << "Enter y: ";
        std::cin >> y;
        bool expressionA1 = x >= 5 && y < 7;
        bool expressionA2 = !((x < 5) || (y >= 7));
        if (expressionA1 == expressionA2) {
            std::cout << "Expressions x >= 5 && y < 7 and !((x < 5) || (y >= 7)) are equivalent." << std::endl;
        } else {
            std::cout << "Expressions x >= 5 && y < 7 and !((x < 5) || (y >= 7)) are not equivalent." << std::endl;
        }
    }
    
    {
        int a, b, g;
        std::cout << "Enter a: ";
        std::cin >> a;
        std::cout << "Enter b: ";
        std::cin >> b;
        std::cout << "Enter g: ";
        std::cin >> g;
        bool expressionB1 = a != b || 5 == g;
        bool expressionB2 = !((a == b) && (5 != g));
        if (expressionB1 == expressionB2) {
            std::cout << "Expressions a != b || 5 == g and !((a == b) && (5 != g)) are equivalent." << std::endl;
        } else {
            std::cout << "Expressions a != b || 5 == g and !((a == b) && (5 != g)) are not equivalent." << std::endl;
        }
    }
    
    {
        int x, y;
        std::cout << "Enter x: ";
        std::cin >> x;
        std::cout << "Enter y: ";
        std::cin >> y;
        bool expressionC1 = !((x <= 8) && (y > 4));
        bool expressionC2 = x > 8 || y <= 4;
        if (expressionC1 == expressionC2) {
            std::cout << "Expressions !((x <= 8) && (y > 4)) and x > 8 || y <= 4 are equivalent." << std::endl;
        } else {
            std::cout << "Expressions !((x <= 8) && (y > 4)) and x > 8 || y <= 4 are not equivalent." << std::endl;
        }
    }
    
    {
        int i, j;
        std::cout << "Enter i: ";
        std::cin >> i;
        std::cout << "Enter j: ";
        std::cin >> j;
        bool expressionD1 = !((i > 4) || (j <= 6));
        bool expressionD2 = i <= 4 && j > 6;
        if (expressionD1 == expressionD2) {
            std::cout << "Expressions !((i > 4) || (j <= 6)) and i <= 4 && j > 6 are equivalent." << std::endl;
        } else {
            std::cout << "Expressions !((i > 4) || (j <= 6)) and i <= 4 && j > 6 are not equivalent." << std::endl;
        }
    }
    
    return 0;
}

