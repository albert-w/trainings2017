#include <iostream>

int
main()
{
    for (int counter = 1; counter <= 5; ++counter) {
        int number;
        std::cout << "Enter number: ";
        std::cin >> number;

        while (number != 0) {
            std::cout << '*';
            --number;
        }
        std::cout << std::endl;
    }

    return 0;
}

