#include <iostream>

int
main()
{
    int counter = 1;
    std::cout << "N\t10*N\t100*N\t1000*N\n" << std::endl;

    while (counter <= 5) {
        std::cout << counter << "\t" << 10 * counter << "\t"
            << 100 * counter << "\t" << 1000 * counter << std::endl;
        counter++;
    }

    return 0;
}

