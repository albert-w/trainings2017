/// Mikayel Ghaziyan
/// 11/10/2017
/// Exercise 2.16

#include <iostream> ///For input and output allowance 

/// Beginning of the main function
int
main()
{   
    int number1, number2; /// Declaring the input numbers
    
    /// Prompting the user to enter the value of the two numbers
    std::cout << "Please enter two numbers: "; 
    std::cout << "First number: ";
    std::cin >> number1;
    std::cout << "Second number (except 0): ";
    std::cin >> number2;
    
    /// Making Calculations
    if (0 == number2) {  /// Condition if the second number is 0 
        std::cout << "Error 1.Division by zero. Cannot devide by zero.Try again        \n";

	return 1; /// Error ending
    }    
    /// If the second number is not 0, then make the calculations
    int sum = number1 + number2;  /// Calculating the sum
    int product = number1 * number2;  /// Calculating the product
    int difference = number1 - number2;  /// Calculating the difference
    int quotient = number1 / number2;  /// Calculating the quotient 

    /// Displaying the result
    std::cout << "The sum of " << number1 << " and " << number2 << " is " << sum          << ".\n";  /// The result of the sum 
    std::cout << "The product of " << number1 << " and " << number2 << " is " <<          product << ".\n";  /// The result of the product
    std::cout << "The difference of " << number1 << " and " << number2 << " is "         << difference << ".\n";  /// The result of subtraction
    std::cout << "The quotient of " << number1 << " and " << number2 << " is "         << quotient << ".\n";  /// The result of division

    return 0; /// The program ended successfully
}  /// End of the main function


/// End of the file

