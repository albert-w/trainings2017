#include <string>

class Invoice
{
public:
    Invoice(int itemNumber, std::string description, int quantity, int price);
    void setItemNumber(int itemNumber);
    void setDescription (std::string description);
    void setQuantity (int quantity);
    void setPrice(int price);
    std::string getDescription();
    int getItemNumber();
    int getQuantity();
    int getPrice();
    int getInvoiceAmount();
private: 
    int partNumber_;
    std::string partDescription_;
    int partQuantity_;
    int partPrice_;
    int partAmount_;
};

